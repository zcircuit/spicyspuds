const { executeTransaction } = require("@algo-builder/algob");
const { types } = require('@algo-builder/web');
import * as algob from "@algo-builder/algob";

async function run(runtimeEnv: algob.types.RuntimeEnv, deployer: algob.types.Deployer) {
    console.log(`starting app delete`);
    const appId = parseInt(process.env.APP_ID || '');
    console.log(`deleting app id`, appId);

    const deleteAppTx = {
        type: types.TransactionType.DeleteApp,
        sign: types.SignType.SecretKey,
        fromAccount: deployer.accountsByName.get('master'),
        appID: appId,
        payFlags: {
            totalFee: 1e3,
        },
        appArgs: [],
    };
    await executeTransaction(deployer, deleteAppTx);
    console.log(`app ${appId} deleted`);
}
module.exports = { default: run };
