import * as algob from "@algo-builder/algob";
import { executeTransaction } from "@algo-builder/algob";
import { types } from '@algo-builder/web';
import { Account } from "algosdk";

async function run(runtimeEnv: algob.types.RuntimeEnv, deployer: algob.types.Deployer) {
    console.log(`opting in`);
    const appId = parseInt(process.env.APP_ID || '');
    console.log(`for app id`, appId);

    const fromAccount = deployer.accountsByName.get(process.env.ACCOUNT_NAME || 'master') as Account
    const optInTx: types.AppCallsParam = {
        type: types.TransactionType.OptInToApp,
        sign: types.SignType.SecretKey,
        fromAccount,
        appID: appId,
        payFlags: {
            totalFee: 1e3,
        },
    };
    await executeTransaction(deployer, optInTx);
    console.log(`${fromAccount.addr} opted into appId ${appId}`);
}
module.exports = { default: run };
