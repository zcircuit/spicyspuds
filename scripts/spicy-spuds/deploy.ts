import * as algob from "@algo-builder/algob";
import { Account } from "algosdk";
import { ForceDeployer } from "../utils/force-deployer";

async function run(
    runtimeEnv: algob.types.RuntimeEnv,
    deployer: algob.types.Deployer
): Promise<void> {
    console.log('Starting deploy of SpicySpudsManager');
    const forceDeployer = new ForceDeployer(deployer);

    const appInfo = await forceDeployer.deployApp('SpicySpudsManager.py', 'noop-clear.teal', {
        globalBytes: 8,
        globalInts: 8,
        localBytes: 8,
        localInts: 8,
        sender: deployer.accountsByName.get('master') as Account,
    }, {});

    console.log(`Deployed SpicySpudsManager: ${JSON.stringify(appInfo, null, 4)}`);
}


module.exports = { default: run };