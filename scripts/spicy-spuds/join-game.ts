import * as algob from "@algo-builder/algob";
import { executeTransaction } from "@algo-builder/algob";
import { types } from '@algo-builder/web';
import { Account } from "algosdk";

async function run(runtimeEnv: algob.types.RuntimeEnv, deployer: algob.types.Deployer) {
    console.log(`starting joinGame`);
    const appId = parseInt(process.env.APP_ID || '');
    console.log(`for app id`, appId);

    const fromAccount = deployer.accountsByName.get(process.env.ACCOUNT_NAME || 'master') as Account
    const joinGameTx: types.AppCallsParam = {
        type: types.TransactionType.CallApp,
        sign: types.SignType.SecretKey,
        fromAccount,
        appID: appId,
        payFlags: {
            totalFee: 1e3,
        },
        appArgs: [
            "str:joinGame",
            `int:${process.env.TEAM_INDEX || 0}`
        ],
    };
    await executeTransaction(deployer, joinGameTx);
    console.log(`${fromAccount.addr} joined game`);
}
module.exports = { default: run };
