import * as algob from "@algo-builder/algob";
import { executeTransaction } from "@algo-builder/algob";
import { types } from '@algo-builder/web';
import { Account } from "algosdk";

async function run(runtimeEnv: algob.types.RuntimeEnv, deployer: algob.types.Deployer) {
    console.log(`starting sendSpuds`);
    const appId = parseInt(process.env.APP_ID || '');
    console.log(`for app id`, appId);

    const spudsSender = deployer.accountsByName.get(process.env.SRC_ACCOUNT_NAME || 'master') as Account;
    const spudsReceiver = deployer.accountsByName.get(process.env.DEST_ACCOUNT_NAME || 'master')!.addr;
    const sendSpudsTx: types.AppCallsParam = {
        type: types.TransactionType.CallApp,
        sign: types.SignType.SecretKey,
        fromAccount: spudsSender,
        appID: appId,
        payFlags: {
            totalFee: 1e3,
        },
        appArgs: [
            "str:sendSpuds",
            `addr:${spudsReceiver}`
        ],
        accounts: [
            spudsReceiver,
        ]
    };
    await executeTransaction(deployer, sendSpudsTx);
    console.log(`${spudsSender.addr} sent spuds to ${spudsReceiver}`);
}
module.exports = { default: run };
