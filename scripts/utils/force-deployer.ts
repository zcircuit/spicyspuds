import * as algob from "@algo-builder/algob";
import { Deployer } from "@algo-builder/algob/build/types";
import { tx, types as rtypes } from "@algo-builder/web";

// Algob doesn't allow deploying outside of 'deploy' phase
//   - Very opinionated... fml, we need to deploy whenever we damn well please
export class ForceDeployer {
    constructor(private deployer: Deployer) { }

    async deployApp(
        approvalProgram: string,
        clearProgram: string,
        flags: rtypes.AppDeploymentFlags,
        payFlags: rtypes.TxParams,
    ): ReturnType<Deployer['deployApp']> {
        const params = await algob.mkTxParams(this.deployer.algodClient, payFlags);
        const app = await this.deployer.ensureCompiled(approvalProgram);
        const clear = await this.deployer.ensureCompiled(clearProgram);
        const approvalProg = new Uint8Array(Buffer.from(app.compiled, "base64"));
        const clearProg = new Uint8Array(Buffer.from(clear.compiled, "base64"));
        const execParam = {
            type: rtypes.TransactionType.DeployApp,
            sign: rtypes.SignType.SecretKey,
            fromAccount: flags.sender,
            approvalProgram: approvalProgram,
            clearProgram: clearProgram,
            approvalProg: approvalProg,
            clearProg: clearProg,
            payFlags: payFlags,
            localInts: flags.localInts,
            localBytes: flags.localBytes,
            globalInts: flags.globalInts,
            globalBytes: flags.globalBytes,
            accounts: flags.accounts,
            foreignApps: flags.foreignApps,
            foreignAssets: flags.foreignAssets,
            appArgs: flags.appArgs,
            note: flags.note,
            lease: flags.lease
        };
        const txn = tx.mkTransaction(execParam as any, params);
        const txId = txn.txID().toString();
        const signedTxn = txn.signTxn(flags.sender.sk);
        const txInfo = await this.deployer.algodClient.sendRawTransaction(signedTxn).do();
        const confirmedTxInfo = await this.deployer.waitForConfirmation(txId);
        const appId = confirmedTxInfo['application-index'];
        const message = `Signed transaction with txID: ${txId}\nCreated new app-id: ${appId}`; // eslint-disable-line @typescript-eslint/restrict-template-expressions
        console.log(message);
        return {
            creator: flags.sender.addr,
            txId: txInfo.txId,
            // confirmedRound: Number(confirmedTxInfo[confirmedRound]),
            confirmedRound: 0,
            appID: Number(appId),
            timestamp: Math.round(+new Date() / 1000),
            deleted: false
        };
    }
}