# https://developer.algorand.org/tutorials/create-private-network/

export ALGORAND_DATA=`pwd`/node_data/PrimaryNode
export SANDBOX_PATH = ~/.algorand-sandbox
export REMOTE_REPO = https://github.com/algorand/sandbox.git

sandbox-up:
# clone sandbox git repo: https://github.com/algorand/sandbox.git and pull
	[ -d $(SANDBOX_PATH) ] || git clone $(REMOTE_REPO) $(SANDBOX_PATH)
	(cd $(SANDBOX_PATH); git reset --hard; git pull;)
	$(SANDBOX_PATH)/sandbox up -v

sandbox-down:
# stops docker containers for algod, indexer and indexer-db
	$(SANDBOX_PATH)/sandbox down

sandbox-clean:
# clean up env (removing stopped docker containers, images)
	$(SANDBOX_PATH)/sandbox clean

sandbox-algod-shell:
# Opens algod container bash shell
	$(SANDBOX_PATH)/sandbox enter algod

sandbox-setup-master-account:
# similar to setup-master-account but with sandbox, goal commands are executed using the ./sandbox file
	$(SANDBOX_PATH)/sandbox goal account import -m "enforce drive foster uniform cradle tired win arrow wasp melt cattle chronic sport dinosaur announce shell correct shed amused dismiss mother jazz task above hospital"
	@$(eval list=$(shell $(SANDBOX_PATH)/sandbox goal account list))
	@$(eval netAddress=$(shell echo $(list) | awk '{print $$2}'))
	$(SANDBOX_PATH)/sandbox goal clerk send -a 2000000000000 -f $(netAddress) -t WWYNX3TKQYVEREVSW6QQP3SXSFOCE3SKUSEIVJ7YAGUPEACNI5UGI4DZCE

# Extracting mnemonic:
# goal -d $(ALGORAND_DATA) account export -a <account address>
