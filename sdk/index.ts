import type { Algodv2 } from "algosdk";
import { algosdk } from './algosdk';

const atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

const confirmedRound = "confirmed-round";

export class SpicySpudsSdk {

    approvalB64 = '';
    clearB64 = '';
    managerAppId: number;

    constructor(
        private algod: Algodv2,
        managerAppId: number | string,
    ) {
        this.managerAppId = typeof managerAppId === 'string'
            ? parseInt(managerAppId, 10)
            : managerAppId;
    }

    async createZixel(signer: SpudsSigner) {
    }

    private async waitForConfirmation(txId: string) {
        const response = await this.algod.status().do();
        let lastround = response["last-round"];
        while (true) {
            const pendingInfo = await this.algod.pendingTransactionInformation(txId).do();
            if (pendingInfo[confirmedRound] !== null && pendingInfo[confirmedRound] > 0) {
                return pendingInfo;
            }
            lastround++;
            await this.algod.statusAfterBlock(lastround).do();
        }
    }

    // address = 32byte base32 address string
    private addressToPk(address: string): Uint8Array {
        return algosdk.decodeAddress(address).publicKey;
    }

    _useProgram(approvalB64: string, clearB64: string) {
        this.approvalB64 = approvalB64;
        this.clearB64 = clearB64;
    }
}

export interface SpudsSigner {
    account: string,
    sign(txns: Uint8Array): Promise<SignedTx>,
    sign(txns: Uint8Array[]): Promise<SignedTx[]>,
    sign(txns: Uint8Array | Uint8Array[]): Promise<SignedTx | SignedTx[]>,
}

export interface SignedTx {
    // Transaction hash 32Bytes
    txID: string;
    // Signed transaction
    blob: Uint8Array;
}
