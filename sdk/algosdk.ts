import type * as _algosdkType from 'algosdk';

export const algosdk: typeof _algosdkType = (globalThis as any).algosdk || require('algosdk');