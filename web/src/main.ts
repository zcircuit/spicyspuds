import App from './App.svelte';

const app = new App({
    target: document.body,
});

function checkForSwSupport() {
    if (!('serviceWorker' in navigator)) {
        throw new Error('No Service Worker support!')
    }
}

async function registerServiceWorker() {
    return await navigator.serviceWorker.register('./bundle.sw.js', { scope: './' });
}

(async () => {
    if (process.env.NODE_ENV === 'production') {
        checkForSwSupport();
        await registerServiceWorker();
    }
})()

window.addEventListener('appinstalled', e => {
    console.log('window.appinstalled', e);
});

export default app;