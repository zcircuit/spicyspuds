import { readable } from 'svelte/store';

export type InstallPrompt = Event & {
    prompt: () => Promise<void>
}

export default {
    installPrompt: readable<InstallPrompt>(null, set => {
        window.addEventListener('beforeinstallprompt', e => set(e as any));
    }),
}
