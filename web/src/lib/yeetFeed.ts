import { first, firstValueFrom, map, merge, scan, shareReplay, skip, Subject, switchMap, tap, timer } from "rxjs";
import { ReceivedYeet, SpudWallet } from "./wallet";

let latestYeetRound;
let forceUpdate$ = new Subject();
const yeetFeed = merge(timer(0, 4000), forceUpdate$).pipe(
    switchMap(() => SpudWallet.getAllReceivedYeets(latestYeetRound)),
    scan((acc, yeets) => {
        yeets.forEach(y => acc.set(y.id, y));
        return acc;
    }, new Map<string, ReceivedYeet>()),
    map(yeetMap => Array.from(yeetMap.values())
        .sort((y1, y2) => y1.txn["confirmed-round"] - y2.txn["confirmed-round"])
    ),
    tap(yeets => latestYeetRound = yeets.slice(-1)[0]?.txn?.["confirmed-round"]),
    shareReplay(1),
)

export function getYeetFeed() {
    return yeetFeed;
}

export function forceUpdateYeetFeed() {
    forceUpdate$.next({});
    return firstValueFrom(yeetFeed.pipe(skip(1), first()))
}