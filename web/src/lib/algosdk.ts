import type * as _algosdkType from 'algosdk';

export const algosdk: typeof _algosdkType = (window as any).algosdk;

