import type { Transaction } from 'algosdk';
import { BehaviorSubject, combineLatest, filter, map, mergeMap, shareReplay, startWith, switchMap, timer } from 'rxjs';
import { algosdk } from './algosdk';
import { EncryptedStorage } from './encryptedStorage';
import { addressToPk, stringToBytes, tick, waitForConfirmation } from './utils';

const managerAddr = parseInt(process.env.SPUDS_MANAGER_ADDR, 10);
let managerStartRound;
console.log('Using SpudManager: ', managerAddr);
export const algodClient = new algosdk.Algodv2(
    process.env.ALGOD_API_KEY,
    process.env.ALGOD_HOST,
    process.env.ALGOD_PORT,
);
const indexerClient = new algosdk.Indexer(
    process.env.INDEXER_API_KEY,
    process.env.INDEXER_HOST,
    process.env.INDEXER_PORT,
);
// export const spudSdk = new SpicySpudsSdk(algodClient, process.env.SPUDS_MANAGER_ADDR);

const storage = EncryptedStorage(() => Promise.resolve('123'));

const SPUD_BALANCE_KEY = 'c3B1ZEJhbGFuY2U=';
const TEAM_KEY = btoa('team');
const START_ROUND_KEY = btoa('startRound');


// hacks hacks hacks
const accInfo$ = new BehaviorSubject<any>({});
const myState$ = timer(0, 4e3).pipe(
    mergeMap(() => combineLatest([accInfo$, SpudWallet.spudState()])),
    filter(Boolean),
    map(([accInfo, state]) => {
        const kvs = state['key-value'] || []
        return {
            spudBalance: kvs.find(kv => kv.key === SPUD_BALANCE_KEY)?.value?.uint || 0,
            team: kvs.find(kv => kv.key === TEAM_KEY)?.value?.uint,
            algoBalance: accInfo.amount,
        } as PlayerState;
    }),
    shareReplay(1),
);

const gameState$ = timer(0, 4e3).pipe(
    switchMap(() => SpudWallet.getGameState()),
    startWith({
        scores: [],
        adjustedScores: [],
        highScore: 0,
    }),
    shareReplay(1),
);


export const SpudWallet = {
    hasWallet() {
        return storage.hasItem('wallet_sk') && !!localStorage.getItem('wallet_pk');
    },
    getAddress() {
        return localStorage.getItem('wallet_pk');
    },
    async createWallet() {
        const account = algosdk.generateAccount();
        await storage.setItem('wallet_sk', JSON.stringify(Array.from(account.sk)));
        localStorage.setItem('wallet_pk', account.addr);

        return account.addr;
    },
    async sign(txn: Transaction) {
        const sk = JSON.parse(await storage.getItem('wallet_sk'));
        return txn.signTxn(Uint8Array.from(sk));
    },
    async accountInfo() {
        const accInfo = await algodClient.accountInformation(this.getAddress()).do();
        accInfo$.next(accInfo);
        return accInfo;
    },
    async needsOptIn() {
        const account = await this.accountInfo();
        return !account['apps-local-state']?.some(ls => ls.id === managerAddr);
    },
    async optIn() {
        const suggestedParams = await algodClient.getTransactionParams().do();
        const txn = algosdk.makeApplicationOptInTxnFromObject({
            suggestedParams,
            from: this.getAddress(),
            appIndex: managerAddr,
        });
        const signedTxn: Awaited<ReturnType<typeof SpudWallet['sign']>> = await this.sign(txn);
        await algodClient.sendRawTransaction(signedTxn).do();
        await waitForConfirmation(algodClient, txn.txID());
    },
    async spudState() {
        const account = await this.accountInfo();

        return account['apps-local-state']?.find(ls => ls.id === managerAddr);
    },
    async spudBalance() {
        const state = await this.spudState();

        if (!state) throw new Error('Not opted in ig');

        return state['key-value'].find(kv => kv.key === SPUD_BALANCE_KEY)?.value?.uint;
    },
    watchMyState() {
        if (!this.hasWallet()) throw new Error('Must create an account first');

        return myState$;
    },
    async getMyState() {
        if (!this.hasWallet()) throw new Error('Must create an account first');

        const state = await SpudWallet.spudState();

        return {
            spudBalance: state['key-value'].find(kv => kv.key === SPUD_BALANCE_KEY)?.value?.uint || 0,
            team: state['key-value'].find(kv => kv.key === TEAM_KEY)?.value?.uint,
        } as PlayerState
    },
    async yeet(dest: YeetConfig, msg: string = '') {
        const { address, team } = dest;
        let addr = address;
        if (!address && team === undefined) throw new Error('You gotta give me something' + address + team);

        if (!address && team !== undefined) {
            const players = await SpudWallet.getAllGameParticipants();
            const playersOnTeamInRandomOrder = players
                .filter(p => p.team === team)
                .sort(() => Math.floor(Math.random() * 3) - 1);

            if (playersOnTeamInRandomOrder.length === 0) throw new Error('No players on this team, ' + team);
            addr = playersOnTeamInRandomOrder[0].sender;
        }

        const suggestedParams = await algodClient.getTransactionParams().do();
        const message = btoa(unescape(encodeURIComponent(msg)))
        const txn = algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams,
            appIndex: managerAddr,
            from: this.getAddress(),
            appArgs: [
                stringToBytes('sendSpuds'),
                addressToPk(addr),
            ],
            accounts: [addr],
            note: stringToBytes(`spcyspdyeet:${addr}:${message}:${dest.myTeam}`),
        });
        const signedTxn: Awaited<ReturnType<typeof SpudWallet['sign']>> = await this.sign(txn);
        await algodClient.sendRawTransaction(signedTxn).do();
        await waitForConfirmation(algodClient, txn.txID());
    },
    async joinGame(team: number, gameId: number = 0) {
        const suggestedParams = await algodClient.getTransactionParams().do();
        const txn = algosdk.makeApplicationNoOpTxnFromObject({
            suggestedParams,
            appIndex: managerAddr,
            from: this.getAddress(),
            appArgs: [
                stringToBytes('joinGame'),
                algosdk.encodeUint64(team),
            ],
            note: stringToBytes(`spcyspd:${gameId}:${team}`)
        });
        const signedTxn: Awaited<ReturnType<typeof SpudWallet['sign']>> = await this.sign(txn);
        await algodClient.sendRawTransaction(signedTxn).do();
        await waitForConfirmation(algodClient, txn.txID());
    },
    async getGameState() {
        const rawState = await algodClient.getApplicationByID(managerAddr).do();
        const globalState = rawState.params["global-state"] || [];
        const state = new Map<string, any>(globalState.map(kv => [atob(kv.key), kv.value]));
        console.log(rawState);

        const scores = [
            state.get('scoreForTeam\x00\x00\x00\x00\x00\x00\x00\x00')?.uint || 0,
            state.get('scoreForTeam\x00\x00\x00\x00\x00\x00\x00\x01')?.uint || 0,
            state.get('scoreForTeam\x00\x00\x00\x00\x00\x00\x00\x02')?.uint || 0,
        ] as number[];
        const total = scores.reduce((acc, curr) => acc + curr, 0);
        const adjustedScores = scores.map(s => (total - s) || 1);

        const highScore = adjustedScores.slice().sort((a, b) => b - a)[0];

        return {
            scores,
            adjustedScores,
            highScore,
        }
    },
    watchGameState() {
        return gameState$;
    },
    async getStartRound(gameId: number = 0) {
        while (managerStartRound === null || managerStartRound === undefined) {
            const result = await algodClient.getApplicationByID(managerAddr).do();
            managerStartRound = (result.params['global-state'] || []).find(kv => kv.key === START_ROUND_KEY)?.value?.uint;
            await tick();
        }

        return managerStartRound;
    },
    async getAllGameParticipants(gameId: number = 0) {
        const round = await SpudWallet.getStartRound();

        const gameJoinTransactions = await indexerClient.searchForTransactions()
            .notePrefix(btoa(`spcyspd:${gameId}`))
            .limit(25000)
            .minRound(round)
            .do();

        return gameJoinTransactions.transactions.map(t => {
            const note = atob(t.note);
            const noteParts = note.split(':');
            const team = parseInt(noteParts[2], 10);
            return ({
                sender: t.sender,
                team,
            });
        }) as GamePlayer[];
    },
    async getAllReceivedYeets(afterRound?: number, receiverAddr?: string, gameId: number = 0): Promise<ReceivedYeet[]> {
        let addr = receiverAddr || SpudWallet.getAddress();
        let round = afterRound || await SpudWallet.getStartRound();

        const yeetTransactions = await indexerClient.searchForTransactions()
            .notePrefix(btoa(`spcyspdyeet:${addr}`))
            .limit(25000)
            .minRound(round)
            .do();

        return yeetTransactions.transactions.map(t => {
            const note = atob(t.note);
            const noteParts = note.split(':');
            let message = '';
            try {
                message = decodeURIComponent(escape(atob(noteParts[2] || '')));
            } catch (e) {
                console.log('meh');
            }

            return ({
                sender: t.sender,
                txn: t,
                message,
                id: t.id,
                fromTeam: parseInt(noteParts[3] || '0', 10),
            }) as ReceivedYeet;
        });
    }
}

export interface GamePlayer {
    sender: string,
    team: 0 | 1 | 2;
}

export interface ReceivedYeet {
    sender: string;
    txn: any,
    message: string;
    id: string;
    fromTeam: number;
}

export interface PlayerState {
    spudBalance: number;
    team: number;
    algoBalance: number;
}

export type YeetConfig =
    { team: number, address?: string, myTeam: number } | { team?: number, address?: string, myTeam: number };

// Trigger build