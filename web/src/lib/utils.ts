import type AlgodClient from "algosdk/dist/types/src/client/v2/algod/algod";
import { algosdk } from "./algosdk";

export function tick(time = 1000) {
    return new Promise(res => setTimeout(res, time));
}

export function stringToBytes(str: string) {
    return Uint8Array.from(str, (c => c.charCodeAt(0)));
}

export function addressToPk(addr) {
    return algosdk.decodeAddress(addr).publicKey;
}

export async function waitForConfirmation(algod: AlgodClient, txId: string) {
    const response = await algod.status().do();
    let lastround = response["last-round"];
    while (true) {
        const pendingInfo = await algod.pendingTransactionInformation(txId).do();
        if (pendingInfo["confirmed-round"] !== null && pendingInfo["confirmed-round"] > 0) {
            return pendingInfo;
        }
        lastround++;
        await algod.statusAfterBlock(lastround).do();
    }
}

export const Spuds = ["🥔", "🍠", "🍆"];
