import { firstValueFrom, Subject } from "rxjs";

const getPortal = () => document.querySelector('#dialogPortal');

let currDialog: any;
const closedDialog = new Subject<{}>();

export const DialogService = {
    open(component: any) {
        const portal = getPortal();
        portal.classList.add('open');
        currDialog = new component({
            target: portal.querySelector('#dialogOutlet'),
        });

        return firstValueFrom(closedDialog);
    },
    close() {
        currDialog.$destroy();
        closedDialog.next({});
        getPortal().classList.remove('open');
    }
}