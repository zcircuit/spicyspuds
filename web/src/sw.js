import { precacheAndRoute } from 'workbox-precaching';

const revision = globalThis.revision;
const extraFilesToPrecache = [];

// __WB_MANIFEST will be replaced with an array of entries at compile-time
const manifest = (self.__WB_MANIFEST || []).concat(extraFilesToPrecache);

precacheAndRoute(manifest)

self.addEventListener('install', (e) => {
    e.waitUntil(self.skipWaiting());
})

self.addEventListener('activate', (e) => {
    // This will be called only once when the service worker is activated.
    console.log('service worker activated');

    e.waitUntil(self.clients.claim().then(() => {
        console.log("Clients Claimed");
    }));
});
