/// <reference types="svelte" />
/// <reference types="algosdk" />

export declare global {
    interface Window { dev: boolean }
    type Awaited<T> = T extends PromiseLike<infer U> ? U : T
}
