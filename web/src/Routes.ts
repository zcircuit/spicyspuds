import { SpudWallet } from "./lib/wallet";
import { navigate, Route } from "./Router/router.service";
import FirstTimeComponent from './Views/FirstTime.svelte';
import GameComponent from './Views/Game.svelte';
import JoinTeamComponent from './Views/JoinTeam.svelte';

const Routes: Route[] = [
    { path: '/play', component: GameComponent },
    { path: '/firstTime', component: FirstTimeComponent },
    { path: '/join', component: JoinTeamComponent },
    { path: '/', execute: () => SpudWallet.hasWallet() ? navigate('/play') : navigate('/firstTime'), component: null }
];

export default Routes;