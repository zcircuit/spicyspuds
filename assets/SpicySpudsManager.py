from pyteal import (App, Bytes, Concat, Cond, Global, Int, Itob, Keccak256,
                    Mode, OnComplete, Or, Reject, Return, ScratchVar, Seq,
                    TealType, Txn, compileTeal, Sha256, Sha512_256, Substring,
                    Approve, Addr, Add, Btoi, Assert, Not, Gt, Minus)

SPUD_BALANCE = Bytes("spudBalance")


def newScore(teamIndexBytes):
    return Add(App.globalGet(Concat(Bytes("scoreForTeam"), teamIndexBytes)), App.globalGet(Concat(Bytes("spudCountForTeam"), teamIndexBytes)))


def onSendSpuds():
    spudReceiver = Txn.application_args[1]
    destinationTeamIndexBytes = Itob(App.localGet(
        Txn.application_args[1], Bytes("team")
    ))
    scoreKey = Concat(Bytes("scoreForTeam"), destinationTeamIndexBytes)
    senderTeamIndexBytes = Itob(App.localGet(
        Txn.sender(), Bytes("team")
    ))
    return Seq([
        Assert(Gt(App.localGet(Txn.sender(), SPUD_BALANCE), Int(0))),
        incrementTeamSpudCount(destinationTeamIndexBytes),
        App.globalPut(
            scoreKey,
            newScore(destinationTeamIndexBytes)
        ),
        App.localPut(
            spudReceiver,
            SPUD_BALANCE,
            Add(Int(1), App.localGet(
                spudReceiver,
                SPUD_BALANCE
            ))
        ),
        App.localPut(
            Txn.sender(),
            SPUD_BALANCE,
            Minus(App.localGet(
                Txn.sender(),
                SPUD_BALANCE
            ), Int(1)),
        ),
        decrementTeamSpudCount(senderTeamIndexBytes),
        Approve()
    ])


def incrementTeamSpudCount(team):
    key = Concat(Bytes("spudCountForTeam"), team)
    return App.globalPut(key, Add(App.globalGet(key), Int(1)))


def decrementTeamSpudCount(team):
    key = Concat(Bytes("spudCountForTeam"), team)
    return App.globalPut(key, Minus(App.globalGet(key), Int(1)))


def SpicySpudsManager():

    oncreate = Seq([
        App.globalPut(Bytes("startRound"), Global.round()),
        App.globalPut(Concat(Bytes("scoreForTeam"), Itob(Int(0))), Int(0)),
        App.globalPut(Concat(Bytes("scoreForTeam"), Itob(Int(1))), Int(0)),
        App.globalPut(Concat(Bytes("scoreForTeam"), Itob(Int(2))), Int(0)),
        App.globalPut(Concat(Bytes("spudCountForTeam"), Itob(Int(0))), Int(0)),
        App.globalPut(Concat(Bytes("spudCountForTeam"), Itob(Int(1))), Int(0)),
        App.globalPut(Concat(Bytes("spudCountForTeam"), Itob(Int(2))), Int(0)),
        Approve(),
    ])

    onOptIn = Seq([
        App.localPut(Txn.sender(), SPUD_BALANCE, Int(0)),
        Approve()
    ])

    currTeam = App.localGetEx(Int(0), Int(0), Bytes("team"))

    onJoinGame = Seq([
        currTeam,
        Assert(Not(currTeam.hasValue())),
        App.localPut(Txn.sender(), SPUD_BALANCE, Int(1)),
        App.localPut(
            Txn.sender(),
            Bytes("team"),
            Btoi(Txn.application_args[1])
        ),
        incrementTeamSpudCount(Txn.application_args[1]),
        Approve(),
    ])

    return Cond(
        [
            Or(
                # Txn.on_completion() == OnComplete.DeleteApplication,
                Txn.on_completion() == OnComplete.UpdateApplication,
                Txn.on_completion() == OnComplete.CloseOut,
                Txn.rekey_to() != Global.zero_address(),
                Txn.close_remainder_to() != Global.zero_address()  # Redundant
            ), Reject()
        ],
        [Txn.on_completion() == OnComplete.DeleteApplication, Approve()],
        [Txn.application_id() == Int(0), oncreate],
        [Txn.on_completion() == OnComplete.OptIn, onOptIn],
        [Txn.application_args[0] == Bytes("sendSpuds"), onSendSpuds()],
        [Txn.application_args[0] == Bytes("joinGame"), onJoinGame],
    )


if __name__ == "__main__":
    print(compileTeal(SpicySpudsManager(), mode=Mode.Application, version=5))
